package com.pfe.accessmanagment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import com.pfe.accessmanagment.model.Agence;
import com.pfe.accessmanagment.Repository.AgenceRepository;
import org.springframework.web.bind.annotation.PathVariable;


@Controller
public class AgenceController {
    @Autowired
    AgenceRepository agenceRepository;


    // Get All
    @RequestMapping(path = "/admin/agence", method = RequestMethod.GET)
    public String getAllagences(Model model) {
        model.addAttribute("agences", agenceRepository.findAll());
        return "agences/list";
    }

    // Render form
    @RequestMapping(path = "/admin/agence/add")
    public String createAgence(Model model) {
        model.addAttribute("agence", new Agence());
        return "agences/edit";
    }

    // Create new agence
    @RequestMapping(path = "/admin/agence/save", method = RequestMethod.POST)
    public String saveAgence(Agence agence) {
        agenceRepository.save(agence);
        return "redirect:/admin/agence";
    }

    // Update agence
    @RequestMapping(path = "/admin/agence/edit/{id}", method = RequestMethod.GET)
    public String editAgence(Model model, @PathVariable("id") Long id) {
        Agence agence = agenceRepository.getOne(id);
        model.addAttribute("agence", agence);
        return "agences/edit";
    }

    // Delete agence
    @RequestMapping(path = "/admin/agence/delete/{id}", method = RequestMethod.GET)
    public String deleteAgence(@PathVariable("id") Long id) {
        agenceRepository.deleteById(id);
        return "redirect:/admin/agence";
    }



}
