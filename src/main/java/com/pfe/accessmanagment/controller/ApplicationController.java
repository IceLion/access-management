package com.pfe.accessmanagment.controller;

import com.pfe.accessmanagment.Repository.AgenceRepository;
import com.pfe.accessmanagment.Repository.ApplicationRepository;
import com.pfe.accessmanagment.model.Agence;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.ui.Model;
import com.pfe.accessmanagment.model.Application;

@Controller
public class ApplicationController{
    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    AgenceRepository agenceRepository;


    // Get All
    @RequestMapping(path = "/admin/application", method = RequestMethod.GET)
    public String getAllApplication(Model model) {
        model.addAttribute("applications", applicationRepository.findAll());
        return "applications/list";
    }

    // Render form
    @RequestMapping(path = "/admin/application/add", method = RequestMethod.GET)
    public String createApplication(Model model) {
        model.addAttribute("application", new Application());
        model.addAttribute("agences", agenceRepository.findAll());
        return "applications/edit";
    }

    // Create new application
    @RequestMapping(path = "/admin/application/save", method = RequestMethod.POST)
    public String saveApplication(Application application) {
        applicationRepository.save(application);
        return "redirect:/admin/application";
    }

    // Update application
    @RequestMapping(path = "/admin/application/edit/{id}", method = RequestMethod.GET)
    public String editApplication(Model model, @PathVariable("id") Long id) {
        Application application = applicationRepository.getOne(id);
        model.addAttribute("agences", agenceRepository.findAll());
        model.addAttribute("application", application);
        return "applications/edit";
    }

    // Delete application
    @RequestMapping(path = "/admin/application/delete/{id}", method = RequestMethod.GET)
    public String deleteApplication(@PathVariable("id") Long id) {
        applicationRepository.deleteById(id);
        return "redirect:/admin/application";
    }

}
