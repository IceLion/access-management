package com.pfe.accessmanagment.controller;
import org.springframework.stereotype.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import org.springframework.ui.Model;

import com.pfe.accessmanagment.model.Agence;
import com.pfe.accessmanagment.Repository.AgenceRepository;
import com.pfe.accessmanagment.exception.ResourceNotFoundException;

@Controller
public class IndexController {

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }
}
