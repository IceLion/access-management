package com.pfe.accessmanagment.Repository;

import com.pfe.accessmanagment.model.Agence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgenceRepository extends JpaRepository<Agence, Long>{
}
