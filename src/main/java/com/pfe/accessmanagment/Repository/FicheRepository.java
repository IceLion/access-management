package com.pfe.accessmanagment.Repository;

import com.pfe.accessmanagment.model.Fiche;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface FicheRepository extends JpaRepository<Fiche, Long>{
}
