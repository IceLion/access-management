package com.pfe.accessmanagment.Repository;

import com.pfe.accessmanagment.model.Archive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface ArchiveRepository extends JpaRepository<Archive, Long>{
}
