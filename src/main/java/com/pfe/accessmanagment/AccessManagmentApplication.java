package com.pfe.accessmanagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class AccessManagmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccessManagmentApplication.class, args);
	}
}
